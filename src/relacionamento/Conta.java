/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relacionamento;

/**
 *
 * @author Aluno
 */
public class Conta {
    private Pessoa titular;
    private int numero;
    private int digito;
    private float saldo;

    public Conta(Pessoa titular, int numero, int digito, float saldo) {
        this.titular = titular;
        this.numero = numero;
        this.digito = digito;
        this.saldo = saldo;
    }
    
    public void setTitular(Pessoa titular){
        this.titular=titular;
    }
    
    public Pessoa getTitular(){
        return titular;
    }
    
    public void setNumero(int numero){
        this.numero=numero;
    }
    
    public int getNumero(){
        return numero;
    }
    
    public void setDigito(int digito){
        this.digito=digito;
    }
    
    public int getDigito(){
        return digito;
    }
    
    public void setSaldo(float saldo){
        this.saldo=saldo;
    }
    public float getFloat(){
        return saldo;
    }
    /**
     *
     * @param v1
     */
 
    public boolean debita(float v1) {
        if(saldo>=v1){
            saldo-=v1;
        return true;
        }
        else{
            return false;
        }
    }
    public void credita(float v2) {
        saldo+=v2;
    }
}