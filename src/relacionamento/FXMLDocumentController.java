/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relacionamento;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author user
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label novoSaldo;
    @FXML
    private Button nomeOk;
    @FXML
    private Button numeroOk;
    @FXML
    private Button digitoOk;
    @FXML
    private Button saldoOk;
    @FXML
    private Button adicionarOk;
    @FXML
    private Button debitarOk;
    @FXML
    private TextField nome;
    @FXML
    private TextField numero;
    @FXML
    private TextField digito;
    @FXML
    private TextField saldo;
    @FXML
    private TextField adicionar;
    @FXML
    private TextField debitar;
    Conta c;

@FXML
    private void salvar() {
        Pessoa p = new Pessoa();
        p.setNome(nome.getText());
        c = new Conta(p, Integer.parseInt(numero.getText()), Integer.parseInt(digito.getText()), Integer.parseInt(saldo.getText()));
        novoSaldo.setText(saldo.getText());
    }
@FXML
    private void adicionar() {
        c.credita(Integer.parseInt(adicionar.getText()));
        novoSaldo.setText(c.getFloat()+"");
}
    @FXML
    private void debitar() {
        if(c.debita(Integer.parseInt(debitar.getText()))){
            System.out.println("Operação concluída com sucesso");
            novoSaldo.setText(c.getFloat()+"");
        }
        else{
            System.out.println("Erro - Saldo insuficiente");
        }
}
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
